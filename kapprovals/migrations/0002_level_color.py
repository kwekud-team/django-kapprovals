# Generated by Django 3.2 on 2021-06-26 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapprovals', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='level',
            name='color',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
