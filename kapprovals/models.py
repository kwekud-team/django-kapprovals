import uuid
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from kapprovals.constants import VALUE_TYPES, OPERATORS


class BaseModel(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    guid = models.UUIDField(null=True, blank=True, unique=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        return super(BaseModel, self).save(*args, **kwargs)


class Level(BaseModel):
    identifier = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=250, null=True, blank=True)
    sort = models.IntegerField(default=0, help_text="Regular sorting")
    completed_name = models.CharField(null=True, blank=True, max_length=100,
                                      help_text="Text that appears after completing action")
    action_groups = models.ManyToManyField('auth.Group', blank=True, help_text="Groups which can perform this action")
    action_button_class = models.CharField(max_length=100, null=True, blank=True)
    action_icon_class = models.CharField(max_length=100, null=True, blank=True)
    color = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('sort',)
        unique_together = ('site', 'identifier')

    def get_completed_name(self):
        return self.completed_name or self.name


class LevelContent(BaseModel):
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.level}: {self.content_type}'

    def save(self, *args, **kwargs):
        self.site = self.level.site
        super().save(*args, **kwargs)

    class Meta:
        unique_together = ('site', 'level', 'content_type')
        ordering = ('content_type', 'level')

    def get_summary(self):
        return self.content_type.model.upper()


class LevelContentStep(BaseModel):
    level_content = models.ForeignKey(LevelContent, on_delete=models.CASCADE)
    related_level_content = models.ForeignKey(LevelContent, on_delete=models.CASCADE,
                                              related_name='relatedlevelcontent')
    step = models.IntegerField(default=0)
    is_required = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.level_content}: {self.related_level_content}'

    class Meta:
        unique_together = ('site', 'level_content', 'related_level_content')

    def save(self, *args, **kwargs):
        self.site = self.level_content.site
        super().save(*args, **kwargs)


class LevelContentCondition(BaseModel):
    level_content = models.ForeignKey(LevelContent, on_delete=models.CASCADE)
    field_name = models.CharField(max_length=50)
    operator = models.CharField(max_length=10, choices=OPERATORS, default='==')
    value = models.CharField(max_length=250, null=True, blank=True)
    value_type = models.CharField(max_length=10, choices=VALUE_TYPES, default='str')
    message = models.CharField(max_length=250)
    sort = models.IntegerField(default=1000)

    def __str__(self):
        return '%s %s' % (self.field_name, self.get_operator_display())

    class Meta:
        ordering = ('sort',)

    def save(self, *args, **kwargs):
        self.site = self.level_content.site
        super().save(*args, **kwargs)


# states: approved, deferred, cancelled
class Approval(BaseModel):
    guid = models.UUIDField()
    identifier = models.CharField(max_length=100)
    level_content = models.ForeignKey(LevelContent, on_delete=models.CASCADE)
    status = models.CharField(max_length=50)

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')
    completed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True,
                                     related_name='%(app_label)s%(class)s_completed_by')
    date_completed = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.pk, self.level_content)

    class Meta:
        ordering = ('-date_created',)

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        return super().save(*args, **kwargs)


class ApprovalRoute(BaseModel):
    approval = models.ForeignKey(Approval, on_delete=models.CASCADE)
    related_approval = models.ForeignKey(Approval, on_delete=models.CASCADE, related_name='related_approval')
    date_completed = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = ('site', 'approval', 'related_approval')


class ContentMeta(BaseModel):
    """
    This model is used to keep track per object. We want to track changes to level. It is especially useful for
    reporting. We track the actual change with datetime in the ApprovalContent model.
    """
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey()
    action = models.CharField(max_length=50)
    date_completed = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.content_object)

    class Meta:
        unique_together = ('site', 'content_type', 'object_id')


class ApprovalContent(BaseModel):
    approval = models.ForeignKey(Approval, on_delete=models.CASCADE)
    content_meta = models.ForeignKey(ContentMeta, on_delete=models.CASCADE)
    action = models.CharField(max_length=50)
    note = models.CharField(max_length=250, null=True, blank=True)
    date_completed = models.DateTimeField(null=True, blank=True)
    completed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        unique_together = ('site', 'approval', 'content_meta')

    def get_completed_name(self):
        return self.approval.level_content.level.completed_name or self.action
    get_completed_name.short_description = "Completed name"
