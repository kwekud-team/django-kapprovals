from django import template

register = template.Library()


@register.inclusion_tag('kapprovals/includes/single_approval_box.html', takes_context=True)
def show_single_approval_box(context, approval_level_info, title='Approvals'):

    return {
        'read_site': approval_level_info['read_site'],
        'write_site': approval_level_info['write_site'],
        'levels': approval_level_info['levels'],
        'content_pks': approval_level_info['content_pks'],
        'title': title
    }