from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.sites.models import Site
from django.views.generic import View, TemplateView
from django.views.generic.detail import DetailView
from django.contrib import messages
from django.shortcuts import get_object_or_404

from kapprovals.models import LevelContent, Approval
from kapprovals.contrib.approvals.base import ApprovalManager


class BaseApprovalView(TemplateView):
    level_content = None
    sites = {}
    content = {}

    def dispatch(self, request, *args, **kwargs):
        self.initialize()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'sites': self.sites,
            'level_content': self.level_content,
            'content': self.content
        })
        return context

    def initialize(self):
        self.sites = self.get_sites()
        self.level_content = self.get_level_content()
        self.content = {
            'pks': ','.join(self.get_content_pks()),
            'queryset': self.get_content_queryset()
        }

    def get_sites(self):
        return {
            'read_site': Site.objects.filter(pk=self.kwargs['read_site_pk']).first(),
            'write_site': Site.objects.filter(pk=self.kwargs['write_site_pk']).first()
        }

    def get_level_content(self):
        return LevelContent.objects.filter(pk=self.kwargs['level_ct_pk']).first()

    def get_content_pks(self):
        pk_str = self.request.GET.get('content_pks', '')
        return pk_str.split(',')

    def get_content_queryset(self):
        model_class = self.level_content.content_type.model_class()
        if model_class:
            return model_class.objects.filter(pk__in=self.get_content_pks())


class ContentInfoView(BaseApprovalView):
    template_name = "kapprovals/content_info.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'content_info': self.get_info()
        })
        return context

    def get_info(self):
        return ApprovalManager(**self.sites).reader.get_multiple_content_level_info(
            user=self.request.user, content_queryset=self.content['queryset'], level=self.level_content.level
        )


class RunApprovalView(BaseApprovalView):
    template_name = "kapprovals/run_approval.html"

    def post(self, request, *args, **kwargs):
        approval = ApprovalManager(**self.sites).writer.run_approval(
            self.request, self.request.user, self.level_content, self.content['queryset'],
            content_meta=self.build_content_meta()
        )
        messages.success(request, "Completed successfully")
        return HttpResponseRedirect(self.get_success_url(approval))

    def get_success_url(self, approval):
        url = reverse('kapprovals:approval_detail', args=(approval.pk,))
        return f'{url}?acp=1'

    def build_content_meta(self):
        meta = {}
        for x in self.content['queryset']:
            meta[x.pk] = {
                'note': self.request.POST.get(f'{x.pk}__note'),
                'action': self.request.POST.get(f'{x.pk}__action') or 'accept',
            }
        return meta

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'page_type': self.request.GET.get('pt'),
            'base_template': self.get_base_template(),
        })
        return context

    def get_base_template(self):
        if self.request.GET.get('pt') == 'full':
            return 'kapprovals/base.html'
        else:
            return 'kapprovals/base_popup.html'


class ApprovalDetailView(DetailView):
    model = Approval

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'auto_close_popup': self.request.GET.get('acp')
        })
        return context


class ApprovalExecuteView(View):

    def get(self, request, *args, **kwargs):
        approval = get_object_or_404(Approval, pk=kwargs['approval_pk'])
        level_content = get_object_or_404(LevelContent, pk=kwargs['level_ct_pk'])
        write_site = approval.site
        read_site = level_content.site

        content_pk_list = approval.approvalcontent_set.values_list('content_meta__object_id', flat=True)
        content_pks = ','.join(map(str, content_pk_list))

        next_approval = ApprovalManager(read_site, write_site=write_site).writer.get_approval(
            level_content, content_pk_list
        )

        if next_approval and next_approval.date_completed:
            url = reverse('kapprovals:approval_detail', args=(next_approval.pk,))
        else:

            url = reverse('kapprovals:run_approval', args=(read_site.pk, write_site.pk, level_content.pk,))
            url = f'{url}?content_pks={content_pks}&pt=full'

        return HttpResponseRedirect(url)
