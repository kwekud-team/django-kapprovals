from kapprovals.contrib.approvals.base import ApprovalManager


def get_single_content_level_info(read_site, user, content_object, write_site=None):
    return ApprovalManager(read_site, write_site=write_site).reader.get_single_content_level_info(user, content_object)


def get_multiple_content_level_info(read_site, user, content_object, level, write_site=None):
    return ApprovalManager(read_site, write_site=write_site).reader.get_multiple_content_level_info(
        user, content_object, level)
