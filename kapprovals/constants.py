from enum import Enum


class KapprovalsK(Enum):
    ACTION_ACCEPT = 'accept'
    ACTION_REJECT = 'reject'
    ACTION_RETURN = 'return'

    VT_INTEGER = 'int'
    VT_STRING = 'str'
    VT_FLOAT = 'float'
    VT_DECIMAL = 'dec'
    VT_BOOLEAN = 'bool'
    VT_DATE = 'date'
    VT_DATETIME = 'datetime'
    VT_FUNCTION = 'function'

    KALERT_SERVICE_NAME = 'approvals'

    APPR_STATE_INIT = 'initialize'
    APPR_STATE_NEW = 'new'
    APPR_STATE_PENDING = 'pending'
    APPR_STATE_COMPLETED = 'completed'



# TYPES = (
#     (KapprovalsK.AL_TYPE_APPROVALS.value, 'Approvals'),
#     (KapprovalsK.AL_TYPE_GROUPS.value, 'Groups'),
#     (KapprovalsK.AL_TYPE_CHECKS.value, 'Checks'),
# )
#
# ACTIONS = (
#     (KapprovalsK.AP_ACTION_APPROVE.value, 'Approve'),
#     (KapprovalsK.AP_ACTION_REJECT.value, 'Reject'),
#     (KapprovalsK.AP_ACTION_TERMINATE.value, 'Terminate'),
# )


OPERATORS = (
    ('==', 'Exact'),
    ('<', 'Less Than'),
    ('<=', 'Less Than or Equal to'),
    ('>', 'Greater Than'),
    ('>=', 'Greater Than or Equal to'),
)

VALUE_TYPES = (
   (KapprovalsK.VT_STRING.value, 'String'),
   (KapprovalsK.VT_INTEGER.value, 'Integer'),
   (KapprovalsK.VT_FLOAT.value, 'Float'),
   (KapprovalsK.VT_DECIMAL.value, 'Decimal'),
   (KapprovalsK.VT_BOOLEAN.value, 'Boolean'),
   (KapprovalsK.VT_DATE.value, 'Date'),
   (KapprovalsK.VT_DATETIME.value, 'DateTime'),
   (KapprovalsK.VT_FUNCTION.value, 'Function'),
)
