from dataclasses import dataclass, field
from typing import List


@dataclass
class IssueItem:
    section: str
    message: List


@dataclass
class LevelActionItem:
    can_run: bool
    is_completed: bool = False
    issues: List[IssueItem] = field(default_factory=list)
