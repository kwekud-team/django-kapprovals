from django.urls import path

from kapprovals import views


app_name = 'kapprovals'


urlpatterns = [
    path("execute/<int:approval_pk>/<int:level_ct_pk>/", views.ApprovalExecuteView.as_view(), name="approval_execute"),
    path("detail/<int:pk>/", views.ApprovalDetailView.as_view(), name="approval_detail"),

    path("approval/run/<int:read_site_pk>/<int:write_site_pk>/<int:level_ct_pk>/", views.RunApprovalView.as_view(),
         name="run_approval"),
    path("content/info/<int:read_site_pk>/<int:write_site_pk>/<int:level_ct_pk>/", views.ContentInfoView.as_view(),
         name="content_info"),
]
