import django.dispatch


kapprovals_pre_run_approval = django.dispatch.Signal()
kapprovals_post_run_approval = django.dispatch.Signal()
kapprovals_post_next_steps = django.dispatch.Signal()
