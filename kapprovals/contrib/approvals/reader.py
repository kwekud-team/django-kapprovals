from django.contrib.contenttypes.models import ContentType

from kommons.utils.generic import get_recursive_attr
from kapprovals.models import LevelContent, LevelContentStep, ApprovalContent, ContentMeta
from kapprovals.constants import KapprovalsK
from kapprovals.dclasses import LevelActionItem, IssueItem


class ApprovalReader:

    def __init__(self, manager):
        self.manager = manager

    def get_single_content_level_info(self, user, content_object):
        content_queryset = content_object._meta.model.objects.filter(pk=content_object.pk)

        levels_dt = self.get_levels(user, content_queryset)

        return {
            'user': user,
            'read_site': self.manager.read_site,
            'write_site': self.manager.write_site,
            'content_pks': self._get_content_pks(content_queryset),
            'levels': levels_dt[content_object.pk]
        }

    def get_multiple_content_level_info(self, user, content_queryset, level):
        return {
            'user': user,
            'read_site': self.manager.read_site,
            'write_site': self.manager.write_site,
            'content_pks': self._get_content_pks(content_queryset),
            'level': level,
            'levels': self.get_levels(user, content_queryset, level=level)
        }

    @staticmethod
    def _get_content_pks(content_queryset):
        pks = content_queryset.values_list('pk', flat=True)
        return ','.join(map(str, pks))

    def get_levels(self, user, content_queryset, level=None):
        """
        :param user:
        :param content_queryset:
        :param level: Needed when running approvals in bulk. It limits running through validations for all levels
        :return:
        """
        content_type = ContentType.objects.get_for_model(content_queryset.model)

        level_content_qs = LevelContent.objects.filter(site=self.manager.read_site, content_type=content_type)
        if level:
            level_content_qs = level_content_qs.filter(level=level)

        content_dt = {}
        for content_object in content_queryset:

            level_xs = []
            for level_content in level_content_qs:
                has_perm = self._user_has_permission(user, level_content)
                has_pending_dependencies = self._has_pending_dependencies(level_content, content_object)
                conditions_passed, conditions_messages = self._passed_conditions(level_content, content_object)
                approval = self.get_approval(level_content, content_object)

                action = LevelActionItem(can_run=True, is_completed=False)
                if approval and approval.date_completed:
                    action.can_run = False
                    action.is_completed = True
                    action.issues.append(
                        IssueItem(section='approval', message=['Already completed'])
                    )

                else:
                    if not has_perm:
                        action.can_run = False
                        action.issues.append(
                            IssueItem(section='permissions',
                                      message=['You do not have permission to perform this action.'])
                        )
                    else:
                        if has_pending_dependencies:
                            action.can_run = False
                            action.issues.append(
                                IssueItem(section='approval',
                                          message=['Approval not possible until previous steps are completed.'])
                            )
                        elif not conditions_passed:
                            action.can_run = False
                            action.issues.append(
                                IssueItem(section='conditions', message=conditions_messages)
                            )

                if action.can_run:
                    action.issues.append(
                        IssueItem(section='conditions', message=['Ready to run'])
                    )

                level_xs.append({
                    'level_content': level_content,
                    'level': level_content.level,
                    'approval': approval,
                    'action': action,
                })
            content_dt[content_object.pk] = level_xs

        return content_dt

    def get_content_meta(self, level_content, content_object, should_update=False, defaults=None):
        content_type = ContentType.objects.get_for_model(content_object)

        content_meta = ContentMeta.objects.get_or_create(
            site=self.manager.write_site,
            content_type=content_type,
            object_id=content_object.id,
            defaults={
                'level': level_content.level,
            })[0]

        if should_update:
            update_fields = ['level', 'date_modified']
            content_meta.level = level_content.level

            defaults = defaults or {}
            for k, v in defaults.items():
                setattr(content_meta, k, v)
                update_fields.append(k)

            content_meta.save(update_fields=update_fields)

        return content_meta

    def get_approval(self, level_content, content_object):
        return ApprovalContent.objects.filter(
            site=self.manager.write_site,
            approval__level_content=level_content,
            content_meta=self.get_content_meta(level_content, content_object)
        ).first()

    def _user_has_permission(self, user, level_content):
        user_groups = user.groups.all()
        action_groups = level_content.level.action_groups.all()

        found = set(user_groups).intersection(action_groups)
        return bool(found)

    def _has_pending_dependencies(self, level_content, content_object):
        dependent_level_ids = LevelContentStep.objects.filter(
            site=self.manager.read_site,
            level_content__content_type=level_content.content_type,
            related_level_content__level=level_content.level,
            is_required=True
        ).values_list('level_content', flat=True)

        required_dependency_qs = LevelContent.objects.filter(pk__in=dependent_level_ids)
        content_meta = self.get_content_meta(level_content, content_object)

        if required_dependency_qs.exists():
            approved_qs = ApprovalContent.objects.filter(
                site=self.manager.write_site,
                approval__level_content__in=required_dependency_qs,
                date_completed__isnull=False,
                content_meta=content_meta,
            )

            return not (required_dependency_qs.count() == approved_qs.count())

        return False

    def _passed_conditions(self, level_content, ext_object):
        messages = []
        passed = True

        for x in level_content.levelcontentcondition_set.filter(is_active=True):
            if x.value_type == KapprovalsK.VT_FUNCTION.value:
                func = getattr(ext_object, x.field_name, None)
                if func:
                    passed, messages = func(self.manager.write_site, x)
            else:
                val = get_recursive_attr(ext_object, x.field_name.split('.'))

                cleaned_val = validate_value(x.value, x.value_type)
                eval_str = '%s %s %s' % (val, x.operator, cleaned_val)
                evaluate = eval('val %s cleaned_val' % x.operator)

                if not evaluate:
                    passed = False
                    messages.append(x.message)

        return passed, messages

    def get_next_level_content_steps(self, level_content):
        return LevelContentStep.objects.filter(site=self.manager.read_site, level_content=level_content)


def validate_value(value, value_type):
    try:
        if value_type == 'int':
            return int(value)
        elif value_type == 'float':
            return float(value)
        elif value_type == 'bool':
            return bool(int(value))
        else:
            return value
    except:
        return None
