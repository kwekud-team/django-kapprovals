

class ApprovalBaseProcessor:

    def __init__(self, **kwargs):
        self.signal = kwargs.get('signal', None)
        self.request = kwargs.get('request', None)
        self.read_site = kwargs.get('read_site', None)
        self.write_site = kwargs.get('write_site', None)
        self.user = kwargs.get('user', None)
        self.level_content = kwargs.get('level_content', None)
        self.content_queryset = kwargs.get('content_queryset', None)
        self.approval = kwargs.get('approval', None)
        self.approval_content_list = kwargs.get('approval_content_list', None)
        self.next_level_content_steps = kwargs.get('next_level_content_steps', None)

    def run(self):
        self.pre_run()

        func_name = f'run_{self.level_content.level.identifier.replace("-", "_")}'
        results = getattr(self, func_name)() if hasattr(self, func_name) else None
        self.post_run(results)

    def pre_run(self):
        pass

    def post_run(self, results):
        pass
