import hashlib
from django.utils import timezone
from django.urls import reverse

# from kalerts.dclasses import IncidentItem, AssignmentItem
# from kalerts.constants import KalertsK
# from kalerts.shortcuts import log_incident, complete_incident

from kapprovals.models import Approval, ApprovalContent, ApprovalRoute
from kapprovals.constants import KapprovalsK
from kapprovals.signals import kapprovals_pre_run_approval, kapprovals_post_run_approval, kapprovals_post_next_steps


class ApprovalWriter:

    def __init__(self, manager):
        self.manager = manager

    def run_approval(self, request, user, level_content, content_queryset, content_meta=None):
        content_meta = content_meta or {}
        self.send_approve_pre_signal(request, user, level_content, content_queryset)

        approval_extra_update = {}
        content_pk_list = content_queryset.values_list('pk', flat=True)
        approval, is_created = self.create_approval(user, level_content, status=KapprovalsK.APPR_STATE_NEW.value,
                                                    content_queryset=content_pk_list)
        if not is_created:
            approval_extra_update.update({'modified_by': user, 'status': KapprovalsK.APPR_STATE_PENDING.value})

        approval_content_objects = []
        all_completed = True

        for x in content_queryset:
            meta = content_meta.get(x.pk, {})
            note = meta.get('note', '')
            action = meta.get('action', 'accept')

            is_complete = action in [KapprovalsK.ACTION_ACCEPT.value, KapprovalsK.ACTION_REJECT.value]
            if is_complete:
                date_completed = timezone.now()
                completed_by = user
            else:
                all_completed = False
                date_completed = None
                completed_by = None

            content_meta = self.manager.reader.get_content_meta(
                level_content, x, should_update=True, defaults={'action': action, 'date_completed': date_completed})

            a_content = ApprovalContent.objects.update_or_create(
                site=self.manager.write_site,
                approval=approval,
                content_meta=content_meta,
                # object_id=x.pk,
                # content_type=level_content.content_type,
                defaults={
                    'action': action,
                    'note': note,
                    'date_completed': date_completed,
                    'completed_by': completed_by
                })[0]
            approval_content_objects.append(a_content)

        if all_completed:
            approval_extra_update.update({'date_completed': timezone.now(), 'completed_by': user,
                                          'status': KapprovalsK.APPR_STATE_COMPLETED.value})
            self.complete_routes(approval)

        Approval.objects.filter(pk=approval.pk).update(**approval_extra_update)

        self.send_approve_post_signal(request, user, level_content, content_queryset, approval,
                                      approval_content_objects)

        # Send alerts only if current approval is completed and there is a next level
        next_level_content_steps = self.manager.reader.get_next_level_content_steps(level_content)
        if all_completed:
            if next_level_content_steps.exists():
                self.create_approval_routes(next_level_content_steps, approval, content_queryset)

            kapprovals_post_next_steps.send(
                sender=content_queryset.model,
                request=request,
                user=user,
                manager=self.manager,
                next_level_content_steps=next_level_content_steps,
                approval=approval,
                content_queryset=content_queryset,
            )

        return approval

    def get_approval(self, level_content, content_pk_list):
        return Approval.objects.filter(
            site=self.manager.write_site,
            level_content=level_content,
            identifier=self.get_identifier(level_content, content_pk_list),
        ).first()

    def create_approval(self, user, level_content, status, content_queryset):
        return Approval.objects.get_or_create(
            site=self.manager.write_site,
            level_content=level_content,
            identifier=self.get_identifier(level_content, content_queryset),
            defaults={
                'status': status,
                'created_by': user,
                'modified_by': user
            })

    def create_approval_routes(self, next_level_content_steps, approval, content_queryset):
        for next_step in next_level_content_steps:
            content_pk_list = content_queryset.values_list('pk', flat=True)
            next_approval = self.create_approval(approval.created_by, next_step.related_level_content,
                                                 status=KapprovalsK.APPR_STATE_INIT.value,
                                                 content_queryset=content_pk_list)[0]

            ApprovalRoute.objects.get_or_create(
                site=self.manager.write_site,
                approval=approval,
                related_approval=next_approval
            )

    def complete_routes(self, approval):
        qs = ApprovalRoute.objects.filter(related_approval=approval)
        if qs.exists():
            qs.update(date_completed=timezone.now())
            # self.completed_incident(approval, qs)

    def get_identifier(self, level_content, content_pk_list):
        key = f'{self.manager.write_site.pk}-{level_content.pk}-{",".join(map(str, content_pk_list))}'
        return hashlib.md5(key.encode()).hexdigest()

    def send_approve_pre_signal(self, request, user, level_content, content_queryset):
        return kapprovals_pre_run_approval.send(
            sender=content_queryset.model,
            request=request,
            read_site=self.manager.read_site,
            write_site=self.manager.write_site,
            user=user,
            level_content=level_content,
            content_queryset=content_queryset,
        )

    def send_approve_post_signal(self, request, user, level_content, content_queryset, approval,
                                 approval_content_objects):
        return kapprovals_post_run_approval.send(
            sender=content_queryset.model,
            request=request,
            read_site=self.manager.read_site,
            write_site=self.manager.write_site,
            user=user,
            level_content=level_content,
            content_queryset=content_queryset,
            approval=approval,
            approval_content_list=approval_content_objects,
        )
