from kapprovals.contrib.approvals.reader import ApprovalReader
from kapprovals.contrib.approvals.writer import ApprovalWriter


class ApprovalManager:
    """
        read_site is used when getting approval levels. We normally define levels in a parent site and have child sites
        inherit from. This is to prevent duplicating levels for every site we create.
        write_site is used when actually committing approvals. write_site must be the actual site that the user is
        using.
    """
    def __init__(self, read_site, write_site=None):
        self.read_site = read_site
        self.write_site = write_site or read_site
        self.reader = ApprovalReader(self)
        self.writer = ApprovalWriter(self)
