from django.contrib import admin
from django.contrib.contenttypes.models import ContentType

from kapprovals.models import (Level, LevelContentStep, LevelContentCondition, LevelContent, Approval, ApprovalContent,
                               ApprovalRoute, ContentMeta)


class LevelContentInline(admin.TabularInline):
    model = LevelContent
    extra = 1
    fields = ['content_type']

    def get_autocomplete_fields(self, request):
        if admin.site.is_registered(ContentType):
            return ['content_type']
        return super().get_autocomplete_fields(request)


@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'name', 'sort', 'completed_name', 'site')
    list_filter = ('site',)
    search_fields = ['identifier', 'name']
    inlines = [LevelContentInline]


class LevelContentConditionInline(admin.TabularInline):
    model = LevelContentCondition
    extra = 1
    fields = ['field_name', 'operator', 'value', 'value_type', 'message', 'sort', 'is_active']


class LevelContentStepInline(admin.TabularInline):
    model = LevelContentStep
    extra = 1
    fk_name = 'level_content'
    fields = ['related_level_content', 'step', 'is_required']
    autocomplete_fields = ['related_level_content']


@admin.register(LevelContent)
class LevelContentAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'level', 'site')
    list_filter = ('site', 'level')
    search_fields = ['level__name', 'content_type__model']
    inlines = [LevelContentStepInline, LevelContentConditionInline]

    def get_autocomplete_fields(self, request):
        if admin.site.is_registered(ContentType):
            return ['content_type']
        return super().get_autocomplete_fields(request)


@admin.register(LevelContentStep)
class LevelContentStepAdmin(admin.ModelAdmin):
    list_display = ('level_content', 'related_level_content', 'step', 'is_required', 'site')
    list_filter = ('site', 'related_level_content')


class ApprovalContentInline(admin.TabularInline):
    model = ApprovalContent
    extra = 1
    fields = ['action', 'note', 'site']


class ApprovalRouteInline(admin.TabularInline):
    model = ApprovalRoute
    extra = 1
    fk_name = 'approval'
    fields = ['related_approval', 'date_completed', 'site']


@admin.register(Approval)
class ApprovalAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'date_created', 'level_content', 'status', 'date_completed', 'completed_by', 'site')
    list_filter = ('site', 'status', 'date_completed',)
    inlines = [ApprovalRouteInline, ApprovalContentInline]


@admin.register(ApprovalContent)
class ApprovalContentAdmin(admin.ModelAdmin):
    list_display = ('approval', 'action', 'date_completed', 'completed_by', 'content_meta', 'site')
    list_filter = ('site', 'date_completed',)


@admin.register(ContentMeta)
class ContentMetaAdmin(admin.ModelAdmin):
    list_display = ('level', 'content_object', 'action', 'date_completed', 'site')
    list_filter = ('site', 'level',)
